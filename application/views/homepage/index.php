
		<!-- TAB CONTENT HOME -->
		<section class="tab-home">
			<h1 class="title-tab">
                <span>What is <strong>AgentSee</strong>?</span>
            </h1>
            <div id="tabs" class="ui-tabs">
	            <ul class="ui-tabs-nav">
	                <li class="ui-state-default ui-state-active">
	                    <a href="#tabs-1">For Agents</a>
	                </li>
	                <li class="ui-state-default">
	                    <a href="#tabs-2">For Buyers</a>
	                </li>
	            </ul>
	            <div id="tabs-1" class="ui-tabs-panel">
	                <video width="480" height="294" class="state-video left" controls>
	                    <!--<iframe width="480" height="294" src="http://www.youtube.com/embed/qQkYis9Q6TY?list=UUS_1xRIDLdRv7CbHObCWwHQ" frameborder="0" allowfullscreen></iframe>-->
	                    <source src="http://www.youtube.com/embed/qQkYis9Q6TY?list=UUS_1xRIDLdRv7CbHObCWwHQ" type="video/mp4">
	                </video>
	                <aside class="tab-content">
	                    <h3>How do we help agents?</h3>
	                    <dl>
	                        <dd>
	                            Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore 
	
	                        </dd>
	                        <dd>
	                            Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo
	                        </dd>
	                        <dd>
	                            Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat
	                        </dd>
	                    </dl>
	                    <div class="s-tabs">
	                        <h4>SEE HOW MANY BUYERS WE HAVE IN YOUR AREA</h4>
	                        <!-- SEARCH -->
	                        <form action="" method="GET" id="form_search">
								<input type="text" name="search" id="small_search_buyer" >
								<input type="submit" class="btn-s-tabs" value="Search" id="search_submit">
							</form>
							<div id="result_form_search"></div>
							<a href="javascript:void(0)" id="search_again" class="btn-s-tabs" >Search Again</a>
	                        <!-- End of SEARCH -->
	                        <div class="s-tabs-regis">
	                            <a href="#" class="btn dark-blue medium">Register for Free</a>
	                            <label><strong>OR</strong></label>
	                            <label><a href="#"><strong>Sign in</strong></a></label>
	                        </div>
	                    </div>
	                </aside>
	            </div>
	            <div id="tabs-2" class="ui-tabs-panel">
	                <video width="480" height="294" class="state-video left" controls>
	                    <!--<iframe width="480" height="294" src="http://www.youtube.com/embed/qQkYis9Q6TY?list=UUS_1xRIDLdRv7CbHObCWwHQ" frameborder="0" allowfullscreen></iframe>-->
	                    <source src="http://www.youtube.com/embed/qQkYis9Q6TY?list=UUS_1xRIDLdRv7CbHObCWwHQ" type="video/mp4">
	                </video>
	                <aside class="tab-content">
	                    <h3>How do we help buyers?</h3>
	                    <dl>
	                        <dd>
	                            Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore 
	
	                        </dd>
	                        <dd>
	                            Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo
	                        </dd>
	                        <dd>
	                            Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat
	                        </dd>
	                    </dl>
	                    <div class="s-tabs">
	                        <h4>SEE HOW MANY PRE-LISTING IN YOUR AREA</h4>
	                        <!-- SEARCH -->
	                        <form action="" method="GET">
								<input type="text" name="search" id="small_search_list" >
								<input type="submit" class="btn-s-tabs" value="Search">
							</form>
							<!-- End of SEARCH -->
	                        <div class="s-tabs-regis">
	                            <a href="#" class="btn dark-blue medium">Register for Free</a>
	                            <label><strong>OR</strong></label>
	                            <label><a href="#"><strong>Sign in</strong></a></label>
	                        </div>
	                    </div>
	                </aside>
	            </div>
	        </div>
		</section>
        <!-- End of TAB CONTENT HOME -->
		
		
		 <script>
		 function reqAjax(ids){
		 $.ajax({
					type: "POST",
					url: "<?php echo base_url().'ajax/search'; ?>",
					data: { name:ids},      
					success:function(data) {
						$( "#form_search" ).hide();	
						$( "#result_form_search" ).show();
						$( "#result_form_search" ).append(data);
					}
					})
		 }
		$(function() {
		$( "#tabs" ).tabs();
		$( "#search_again" ).hide();
		$( "#search_submit" ).click(function() {
			reqAjax($('#small_search_buyer').val());
			$( "#search_again" ).fadeIn();
			return false;
		});
		$( "#search_again" ).click(function() {
			$( "#search_again" ).hide();
			$( "#form_search" ).show();	
			$( "#result_form_search" ).hide();
			$( "#result_form_search" ).empty();
		});	
		$( "#small_search_buyer" ).autocomplete({ 
			    source:'<?php echo base_url().'ajax/lists'; ?>',
				minLength:3,
				select: function ( event, ui) {
					var ids = document.getElementById("small_search_buyer");
					//ids.value = ui.item.id;
					// request ajax
					reqAjax(ids.value);
		    }
		});
		});
		</script>
		<!--end of tabbed content-->
	</div>
    <div class="bg1"></div>
    <div class="bg2"></div>
</div>
<!-- END og Content Home, Video Tabs -->

<!-- TESTIMONIAL Content and Ads at Home -->
<div id="content" class="testimonial">
    <div class="wrap">
        <!-- Section Testimonial -->
        <section class="testi">
            <span class="ico">
                <i class="testi-ico"></i>
            </span>
            <!-- Agent Testimonial -->
            <div class="left testi-box agent-testi">
                <h2>What are agents saying?</h2>
                <div id="showcase" class="showcase">
                    <div class="showcase-slide showcase-content-wrapper">
                        <div class="showcase-content-wrapper">
                            <i class="quote"></i>
                            <p>
                                AgentSee is the only place I can sell my vendors listing for the best price, super quick, and with low cost to my vendor
                            </p>
                            <p class="testi-name">
                                John Agent
                            </p>
                        </div>
                    </div>
                    <div class="showcase-slide">
                    	<i class="quote"></i>
                        <p>
                            Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.
                        </p>
                        <p class="testi-name">
                            Bob Agent
                        </p>
                    </div>
                </div>
            </div>
            <!-- End of Agent Testimonial -->
            
            <!-- Buyer Testimonial -->
            <div class="right testi-box buyer-testi">
                <h2>What are buyers saying?</h2>
                <div id="showcase2" class="showcase">
                    <div class="showcase-slide showcase-content-wrapper">
                        <div class="showcase-content-wrapper">
                            <i class="quote"></i>
                            <p>
                                My agent sold my home with AgentSee in less than a week and got far more than I would have haped, with next to no marketing fees
                            </p>
                            <p class="testi-name">
                                Bob Vendor
                            </p>
                        </div>
                    </div>
                    <div class="showcase-slide">
                    	<i class="quote"></i>
                        <p>
                            Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.
                        </p>
                        <p class="testi-name">
                            Ront Vendor
                        </p>
                    </div>
                </div>
            </div>
            <!-- End of Buyer Testimonial -->
        </section>
        <!-- End of Section Testimonial -->
        
        <!-- Section Advertisement -->
        <section class="ads">
            <div class="box-ads left">
                <img src="<?php echo base_url(); ?>assets/images/ads.jpg" />
            </div>
            <div class="box-ads right">
                <img src="<?php echo base_url(); ?>assets/images/ads.jpg" />
            </div>
        </section>
        <!-- End of Section Advertisement -->
    </div>
</div>
<!-- End of TESTIMONIAL Content and Ads at Home -->
<div class="clear"></div>

<!--search-->
<script type="text/javascript">
$(document).ready(function(){
$( "#result_form_search" ).hide();
  $("#showcase").awShowcase(
	{
		content_width:			380,
		content_height:			120,
		fit_to_parent:			false,
		auto:					true,
		interval:				3000,
		continuous:				true,
		loading:				true,
		tooltip_width:			200,
		tooltip_icon_width:		32,
		tooltip_icon_height:	32,
		tooltip_offsetx:		18,
		tooltip_offsety:		0,
		arrows:					true,
		buttons:				true,
		btn_numbers:			true,
		keybord_keys:			true,
		mousetrace:				false, /* Trace x and y coordinates for the mouse */
		pauseonover:			true,
		stoponclick:			false,
		transition:				'hslide', /* hslide/vslide/fade */
		transition_delay:		0,
		transition_speed:		600,
		show_caption:			'onload', /* onload/onhover/show */
		thumbnails:				false,
		thumbnails_position:	'inside-first', /* outside-last/outside-first/inside-last/inside-first */
		thumbnails_direction:	'vertical', /* vertical/horizontal */
		thumbnails_slidex:		1, /* 0 = auto / 1 = slide one thumbnail / 2 = slide two thumbnails / etc. */
		dynamic_height:			false, /* For dynamic height to work in webkit you need to set the width and height of images in the source. Usually works to only set the dimension of the first slide in the showcase. */
		speed_change:			true, /* Set to true to prevent users from swithing more then one slide at once. */
		viewline:				false, /* If set to true content_width, thumbnails, transition and dynamic_height will be disabled. As for dynamic height you need to set the width and height of images in the source. */
		custom_function:		null /* Define a custom function that runs on content change */
	});
	$("#showcase2").awShowcase(
	{
		content_width:			380,
		content_height:			120,
		fit_to_parent:			false,
		auto:					true,
		interval:				3000,
		continuous:				true,
		loading:				true,
		tooltip_width:			200,
		tooltip_icon_width:		32,
		tooltip_icon_height:	32,
		tooltip_offsetx:		18,
		tooltip_offsety:		0,
		arrows:					true,
		buttons:				true,
		btn_numbers:			true,
		keybord_keys:			true,
		mousetrace:				false, /* Trace x and y coordinates for the mouse */
		pauseonover:			true,
		stoponclick:			false,
		transition:				'hslide', /* hslide/vslide/fade */
		transition_delay:		0,
		transition_speed:		600,
		show_caption:			'onload', /* onload/onhover/show */
		thumbnails:				false,
		thumbnails_position:	'inside-first', /* outside-last/outside-first/inside-last/inside-first */
		thumbnails_direction:	'vertical', /* vertical/horizontal */
		thumbnails_slidex:		1, /* 0 = auto / 1 = slide one thumbnail / 2 = slide two thumbnails / etc. */
		dynamic_height:			false, /* For dynamic height to work in webkit you need to set the width and height of images in the source. Usually works to only set the dimension of the first slide in the showcase. */
		speed_change:			true, /* Set to true to prevent users from swithing more then one slide at once. */
		viewline:				false, /* If set to true content_width, thumbnails, transition and dynamic_height will be disabled. As for dynamic height you need to set the width and height of images in the source. */
		custom_function:		null /* Define a custom function that runs on content change */
	});
});
</script>