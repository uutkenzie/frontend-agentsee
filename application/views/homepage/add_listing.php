<script>
// GOOGLE API
var placeSearch, autocomplete;
var componentForm = {
  street_number: 'short_name',
  route: 'long_name',
  locality: 'long_name',
  administrative_area_level_1: 'short_name',
  country: 'long_name',
  postal_code: 'short_name'
};

function initialize() {
  // Create the autocomplete object, restricting the search
  // to geographical location types.
  autocomplete = new google.maps.places.Autocomplete(
      /** @type {HTMLInputElement} */(document.getElementById('form_location')),
      { types: ['geocode'] });
  // When the user selects an address from the dropdown,
  // populate the address fields in the form.
  google.maps.event.addListener(autocomplete, 'place_changed', function() {
    fillInAddress();
  });
}

// [START region_fillform]
function fillInAddress() {
  // Get the place details from the autocomplete object.
  var place = autocomplete.getPlace();

  for (var component in componentForm) {
    document.getElementById(component).value = '';
    document.getElementById(component).disabled = false;
  }

  // Get each component of the address from the place details
  // and fill the corresponding field on the form.
  for (var i = 0; i < place.address_components.length; i++) {
    var addressType = place.address_components[i].types[0];
    if (componentForm[addressType]) {
      var val = place.address_components[i][componentForm[addressType]];
      document.getElementById(addressType).value = val;
    }
  }
}
// [END region_fillform]

// [START region_geolocation]
// Bias the autocomplete object to the user's geographical location,
// as supplied by the browser's 'navigator.geolocation' object.
function geolocate() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
      var geolocation = new google.maps.LatLng(
          position.coords.latitude, position.coords.longitude);
      autocomplete.setBounds(new google.maps.LatLngBounds(geolocation,
          geolocation));
    });
  }
}
// [END region_geolocation]
</script>

<script type="text/javascript">
     function readURL(input,index) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#img_'+index)
                        .attr('src', e.target.result)
                        .width(100)
                        .height(100);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }
var i=3;
     function show() {
            document.getElementById("img_"+i).style.display = 'block';
			document.getElementById("form_pic_"+i).style.display = 'block';
			i++;
			if(i==6)document.getElementById("addphoto").style.display = 'none';
        }

$(function() {
var lastSel = $("#form_pricemin option:selected");
$( "#form_pricemin" ).change(function() {
	var e = document.getElementById("form_pricemin");
	var min = e.options[e.selectedIndex].value;
	var f = document.getElementById("form_pricemax");
	var max = f.options[f.selectedIndex].value;
	if(min == 'max' ){
		e.options[e.options.selectedIndex].selected = false;
	}
	if(min > max ){
		e.options[e.options.selectedIndex].selected = false;
	}
});
$( "#form_pricemax" ).change(function() {
	var e = document.getElementById("form_pricemax");
	var max = e.options[e.selectedIndex].value;
	var f = document.getElementById("form_pricemin");
	var min = f.options[f.selectedIndex].value;
	if(max < min ){
		e.options[e.options.selectedIndex].selected = false;
	}
});
});
</script>
<?php
echo $this->formgen->render();
?>
</div> <!--closing top div-->