<script>
// GOOGLE API
var placeSearch, autocomplete;
var componentForm = {
  street_number: 'short_name',
  route: 'long_name',
  locality: 'long_name',
  administrative_area_level_1: 'short_name',
  postal_code: 'short_name'
};

function initialize() {
  // Create the autocomplete object, restricting the search
  // to geographical location types.
  autocomplete = new google.maps.places.Autocomplete(
      /** @type {HTMLInputElement} */(document.getElementById('form_location')),
      { types: ['geocode'] });
  // When the user selects an address from the dropdown,
  // populate the address fields in the form.
  google.maps.event.addListener(autocomplete, 'place_changed', function() {
    fillInAddress();
  });
}

// [START region_fillform]
function fillInAddress() {
  // Get the place details from the autocomplete object.
  var place = autocomplete.getPlace();

  //for (var component in componentForm) {
  //  document.getElementById(component).value = '';
  //  document.getElementById(component).disabled = false;
  //}

  // Get each component of the address from the place details
  // and fill the corresponding field on the form.
  for (var i = 0; i < place.address_components.length; i++) {
    var addressType = place.address_components[i].types[0];
    if (componentForm[addressType]) {
      var val = place.address_components[i][componentForm[addressType]];
	  var fillid;
	  if(addressType == 'street_number') fillid = "form_street_number";
	  if(addressType == 'route') fillid = "form_street_address";
	  if(addressType == 'locality')
	  {
		fillid = "form_city";
		if(val != ''){
			override(val);
		}
	  } 
	  /**PLEASE OVERRRIDE LOCALLITY**/
	  if(addressType == 'administrative_area_level_1') fillid = "form_state";
	  if(addressType == 'postal_code') fillid = "form_zip";
      document.getElementById(fillid).value = val;
	  //document.getElementById(addressType).value = val;
	  
    }
  }
}
// [END region_fillform]

// [START region_geolocation]
// Bias the autocomplete object to the user's geographical location,
// as supplied by the browser's 'navigator.geolocation' object.
function geolocate() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
      var geolocation = new google.maps.LatLng(
          position.coords.latitude, position.coords.longitude);
      autocomplete.setBounds(new google.maps.LatLngBounds(geolocation,
          geolocation));
    });
  }
}
// [END region_geolocation]
</script>

<script type="text/javascript">
 function override(value){
 $.ajax({
			type: "POST",
			url: "<?php echo base_url().'ajax/getSuburb'; ?>",
			data: { name:value},      
			success:function(data) {
				document.getElementById("form_id_suburb").value = data;
				alert(data);
			}
			})
 }
     function readURL(input,index) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#img_'+index)
                        .attr('src', e.target.result)
                        .width(100)
                        .height(100);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }
var i=3;
     function show() {
            document.getElementById("img_"+i).style.display = 'block';
			document.getElementById("form_pic_"+i).style.display = 'block';
			i++;
			if(i==6)document.getElementById("addphoto").style.display = 'none';
        }

$(function() {
// prevent submit on enter
$(document).ready(function() {
  $(window).keydown(function(event){
    if(event.keyCode == 13) {
      event.preventDefault();
      return false;
    }
  });
  $( ".hide" ).hide();
});
// radio button for price
$('#between').click(function() {
   if($('#between').is(':checked')) 
   { 
		$('.price_section_2').hide();
		$('.price_section_1').show();
   }
});
$('#above').click(function() {
   if($('#above').is(':checked')) 
   { 
		$('.price_section_1').hide();
		$('.price_section_2').show();
   }
});
//radio button selection yes or no
$('#yes_1').click(function() {
   if($('#yes_1').is(':checked')) 
   { 
		$('#form_browse1').show();
   }
});
$('#no_1').click(function() {
   if($('#no_1').is(':checked')) 
   { 
		$('#form_browse1').hide();
   }
});
$('#yes_2').click(function() {
   if($('#yes_2').is(':checked')) 
   { 
		$('#form_browse2').show();
   }
});
$('#no_2').click(function() {
   if($('#no_2').is(':checked')) 
   { 
		$('#form_browse2').hide();
   }
});
$('#yes_3').click(function() {
   if($('#yes_3').is(':checked')) 
   { 
		$('#form_browse3').show();
   }
});
$('#no_3').click(function() {
   if($('#no_3').is(':checked')) 
   { 
		$('#form_browse3').hide();
   }
});
$('#yes_4').click(function() {
   if($('#yes_4').is(':checked')) 
   { 
		$('#form_browse4').show();
   }
});
$('#no_4').click(function() {
   if($('#no_4').is(':checked')) 
   { 
		$('#form_browse4').hide();
   }
});
//SECTION 5 COMPARABLE PROPERTY
var form= "";
$('#yes_5').click(function() {
   if($('#yes_5').is(':checked')) 
   { 
		$('#comparable_property').show();
		$('#c_h').append($('.c_header').html());
		$('#c_f').append($('#form_comparable').html());
		$('#c_a').append($('.add_another_comparable').html());
		$('#form_comparable').empty();
		$('.c_header').empty();
		$('.add_another_comparable').empty();
   }
});
$('#no_5').click(function() {
   if($('#no_5').is(':checked')) 
   { 
		$('#comparable_property').hide();
   }
});
$('#c_a').click(function() {
  var filter = new RegExp("(0[123456789]|10|11|12)([/])([0-9][0-9])");
  
	if($('#c_address').val() !='' && filter.test($('#c_sold').val()) && !isNaN($('#c_price').val()) && $('#c_price').val() !='' && $('div#comparable_property').length <=3){
		var content="<div id='comparable_property_id'><div class='c_address'>"+$('#c_address').val()+"</div><div class='c_sold'>"+$('#c_sold').val()+"</div><div class='c_price'>"+$('#c_price').val()+"</div><div class='c_type'>"+$('#c_type').val()+"</div><div class='c_bedroom'>"+$('#c_bedroom').val()+"</div><div class='c_bathroom'>"+$('#c_bathroom').val()+"</div><div class='c_carspot'>"+$('#c_carspot').val()+"</div><div class='c_remove'><img src='' class='c_img'/></div></div><br>";
		$('#c_address').val(''); $('#c_sold').val(''); $('#c_price').val(''); $('#c_type').val('Apartment'); $('#c_bedroom').val(1); $('#c_bathroom').val(1); $('#c_carspot').val(1);
		$('#c_c').append(content);
	} 
	if($('#c_address').val() =='') {
		$('#c_address').val('');
		$("#c_address").attr("placeholder", "please insert address");
	}
	if(filter.test($('#c_sold').val()) == false){
		$('#c_sold').val('');
		$("#c_sold").attr("placeholder", "mm/yy");
	} 
	if(isNaN($('#c_price').val()) || $('#c_price').val() ==''){
		$('#c_price').val('');
		$("#c_price").attr("placeholder", "ex: 90000");
	} 
});
// onclick, remove selected comparable property
$("#c_c").on('click', '.c_remove', function() {
	$(this).parent().remove(); 
});

// FORM BROWSE 1-4 INPUT FILE //
//NO 1
$("#form_browse1").change(function (){
	 $(this).parent().append("<div>"+$("#form_browse1").val()+"<div id='remove_file_1'>remove</div></div>");
	 $("#form_browse1").hide();
	 $(".s_1").hide();
});
$(".inline").on('click', '#remove_file_1', function() {
    $("#form_browse1").val('');
	$('input:radio[name=section_1]').filter('[value=0]').prop('checked', true);
	$(".s_1").show();
	$(this).parent().remove(); 
});
// NO 2
$("#form_browse2").change(function (){
   	$(this).parent().append("<div>"+$("#form_browse2").val()+"<div id='remove_file_2'>remove</div></div>");
	$("#form_browse2").hide();
	$(".s_2").hide();
});
$(".inline").on('click', '#remove_file_2', function() {
    $("#form_browse2").val('');
	$('input:radio[name=section_2]').filter('[value=0]').prop('checked', true);
	$(".s_2").show();
	$(this).parent().remove(); 
});
// NO 3
$("#form_browse3").change(function (){
    $(this).parent().append("<div>"+$("#form_browse3").val()+"<div id='remove_file_3'>remove</div></div>");
	$("#form_browse3").hide();
	$(".s_3").hide();
});
$(".inline").on('click', '#remove_file_3', function() {
    $("#form_browse3").val('');
	$('input:radio[name=section_3]').filter('[value=0]').prop('checked', true);
	$(".s_3").show();
	$(this).parent().remove(); 
});
// NO 4
$("#form_browse4").change(function (){
    $(this).parent().append("<div>"+$("#form_browse4").val()+"<div id='remove_file_4'>remove</div></div>");
	$("#form_browse4").hide();
	$(".s_4").hide();
});
$(".inline").on('click', '#remove_file_4', function() {
    $("#form_browse4").val('');
	$('input:radio[name=section_4]').filter('[value=0]').prop('checked', true);
	$(".s_4").show();
	$(this).parent().remove(); 
});

// DROPDOWN SELECT PRICE
var lastSel = $("#form_pricemin option:selected");
$( "#form_pricemin" ).change(function() {
	var e = document.getElementById("form_pricemin");
	var min = e.options[e.selectedIndex].value;
		$("#form_pricemax option").each(function()
		{
			if( parseInt($(this).val()) <= parseInt(min)){
				$("#form_pricemax option[value="+$(this).val()+"]").attr('disabled','disabled');// disable max
			} else{ $("#form_pricemax option[value="+$(this).val()+"]").removeAttr('disabled');  }
		});
});

$( "#form_pricemax" ).change(function() {
	var e = document.getElementById("form_pricemax");
	var max = e.options[e.selectedIndex].value;
	$("#form_pricemin option").each(function()
		{
			if( parseInt($(this).val()) >= parseInt(max)){
				$("#form_pricemin option[value="+$(this).val()+"]").attr('disabled','disabled');// disable max
			}else $("#form_pricemin option[value="+$(this).val()+"]").removeAttr('disabled');
		});
});
$( "#form_city" ).autocomplete({ 
	    source:'<?php echo base_url(); ?>ajax/lists',
		minLength:3,
		select: function ( event, ui) {
			 var selectedObj = ui.item;
			 document.getElementById("form_state").value = selectedObj.state;
			 document.getElementById("form_zip").value = selectedObj.postcode;
			 document.getElementById("form_id_suburb").value = selectedObj.suburb_id;
	
    }
});

$( "#form_location" ).bind("enterKey",function(e) {
	return false;
});
  $( "#openbox" ).click(function() {
	$( ".hide" ).show();
});
});
</script>

<script type="text/javascript">
//handling onload page
$(document).ready(function(){
var setPrice = "<?php if(isset($setPriceChckbx)) echo $setPriceChckbx; else echo ".price_section_2"; ?>";
$(setPrice).hide();
$('#form_browse1').hide();
$('#form_browse2').hide();
$('#form_browse3').hide();
$('#form_browse4').hide();
$('#form_comparable').hide();
$('.add_another_comparable').hide();
$('.c_header').hide();
});
</script>
<h4>Add a listing</h4>
	ADD DETAILS > FINISH AND SEND ALERTS TO BUYER
<h6>BASIC</h6>
<?php
echo $this->formgen->render();
?>
<div class="c_header">
<div class="c_address">Address</div>
<div class="c_sold">Sold(mm/yy)</div>
<div class="c_price">Price</div>
<div class="c_type"><img src="" class="c_img"/></div>
<div class="c_bedroom"><img src="" class="c_img"/></div>
<div class="c_bathroom"><img src="" class="c_img"/></div>
<div class="c_carspot"><img src="" class="c_img"/></div><br>
</div>
<div id="form_comparable">
<input id="c_address" /><input id="c_sold"/> $<input id="c_price"/>
<select id="c_type">
  <option value="Apartment">Apartment</option>
  <option value="House">House</option>
  <option value="Townhouse">Townhouse</option>
</select>
<select id="c_bedroom">
  <option value="1">1</option>
  <option value="2">2</option>
  <option value="3">3</option>
  <option value="1">4</option>
  <option value="2">5</option>
  <option value="3">6</option>
  <option value="1">7</option>
  <option value="2">8</option>
  <option value="3">9</option>
  <option value="3">10+</option>
</select>
<select id="c_bathroom">
  <option value="1">1</option>
  <option value="2">2</option>
  <option value="3">3</option>
  <option value="1">4</option>
  <option value="2">5</option>
  <option value="3">6</option>
  <option value="1">7</option>
  <option value="2">8</option>
  <option value="3">9</option>
  <option value="3">10+</option>
</select>
<select id="c_carspot">
  <option value="1">1</option>
  <option value="2">2</option>
  <option value="3">3</option>
  <option value="1">4</option>
  <option value="2">5</option>
  <option value="3">6</option>
  <option value="1">7</option>
  <option value="2">8</option>
  <option value="3">9</option>
  <option value="3">10+</option>
</select>
</div>
<div class="add_another_comparable"><img src="" class="c_img"/>Add another</div>
</div> <!--closing top div-->