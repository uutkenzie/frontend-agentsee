<?php
echo doctype('html5');
echo '<html><head>';
echo '<title>'.(empty($title) ? '' : $title.' - ').'Importing System by RealSauce</title>';
?> <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/css/admin.css" />
<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.css" />
<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/css/jquery-ui-1.8.2.custom.css" />
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-1.4.2.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-ui-1.8.2.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-templ.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.validate.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.dataTables.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/border.corner.js"></script>

<?php
echo link_tag('favicon.ico', 'shortcut icon', 'image/ico');
echo '<!--[if lte IE 8]><script type="text/javascript" src="'.site_url('script.ie.js').'"></script><![endif]-->';
//echo '<script type="text/javascript" src="'.site_url('script.js').'"></script>';
?>
</head>
<body>
<div id="top">
<div id="header">
<div class="title_2"><a href="<?php echo base_url();?>">AGENTSEE</a></div>
</div>