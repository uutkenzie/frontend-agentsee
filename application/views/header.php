<?php
echo doctype('html5');
echo '<html><head>';
echo '<title>'.(empty($title) ? '' : $title.' - ').'Importing System by RealSauce</title>';
//echo link_tag('style.css');
?> <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style.css" />
<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/css/jquery-ui-1.8.2.custom.css" />
<link type="text/css" rel="stylesheet" media="screen" href="<?php echo base_url(); ?>assets/css/screenlayout.css" />
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-ui-1.8.2.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-templ.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.validate.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.dataTables.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/border.corner.js"></script>
		<link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500">
		<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places"></script>
		<!--showcase slider-->
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/slide.css" />
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.aw-showcase.js"></script>
		<!--toke input for registration_step_2-->
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.tokeninput.js"></script>
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/token-input-facebook.css" type="text/css" />

<?php
echo link_tag('favicon.ico', 'shortcut icon', 'image/ico');
echo '<!--[if lte IE 8]><script type="text/javascript" src="'.site_url('script.ie.js').'"></script><![endif]-->';
//echo '<script type="text/javascript" src="'.site_url('script.js').'"></script>';
?>
</head>
<body onload="initialize()">

<!-- HEADER -->
<header>
    <div class="wrap">
        <div class="logo">
            <img src="<?php echo base_url(); ?>/assets/images/logo.png" />
            <h4>Australia's pre-market real estate site. Proudly supporting Agents, Buyers, and Sellers in achieving their goals</h4>
        </div>
    </div>
</header>
<!-- END OF HEADER -->

<!-- CONTENT -->
<div id="content" class="home">
    <div class="wrap">
