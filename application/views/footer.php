
<!-- FOOTER -->
	    <footer>
	        <div class="wrap">
	            <dl>
	                <dd>
	                    <h4>About Us</h4>
	                    <ul>
	                        <li><a href="#">Who are we?</a></li>
	                        <li><a href="#">Why AgentSee?</a></li>
	                        <li><a href="#">Contact us</a></li>
	                        <li><a href="#">Terms & Conditions</a></li>
	                    </ul>
	                </dd>
	                <dd>
	                    <h4>Homes By Location</h4>
	                    <ul class="left">
	                        <li><a href="#">NSW</a></li>
	                        <li><a href="#">QLD</a></li>
	                        <li><a href="#">VIC</a></li>
	                        <li><a href="#">WA</a></li>
	                    </ul>
	                    <ul class="left">
	                        <li><a href="#">SA</a></li>
	                        <li><a href="#">ACT</a></li>
	                        <li><a href="#">TAS</a></li>
	                        <li><a href="#">NT</a></li>
	                    </ul>
	                </dd>
	                <dd>
	                    <h4>Buyers By Location</h4>
	                    <ul class="left">
	                        <li><a href="#">NSW</a></li>
	                        <li><a href="#">QLD</a></li>
	                        <li><a href="#">VIC</a></li>
	                        <li><a href="#">WA</a></li>
	                    </ul>
	                    <ul class="left">
	                        <li><a href="#">SA</a></li>
	                        <li><a href="#">ACT</a></li>
	                        <li><a href="#">TAS</a></li>
	                        <li><a href="#">NT</a></li>
	                    </ul>
	                </dd>
	            </dl>
	        </div>
	        <div class="copyright">
	            <p class="wrap">Copyright &copy; 2014 AgentSee</p>
	        </div>
	    </footer>
        <!-- END OF FOOTER -->
	</body>

</html>