<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Properties_model extends CI_Model {
	
	
	function __construct() {
        parent::__construct();
    }
	
	//save data
	function add($data = array())
	{
		$this->db->insert('properties', $data);
		return $this->db->insert_id();
	}
	// save data to table features_properties
	function addPropertyFeature($data = array())
	{
		$this->db->insert('features_properties', $data);
		return $this->db->insert_id();
	}
	
	function getPropertiesCount()
	{
		return $this->db->get('properties')->num_rows();
	}
	
	function getList($perpage, $offset)
	{
		$this->db->limit($perpage,$offset);
		return $this->db->get('properties');
	}
}