<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class User_model extends CI_Model {
	
	
	function __construct() {
        parent::__construct();
    }
	
	// get user data
	function getUser($id = 0, $where = array())
	{
		if ($id > 0)
			$this->db->where('id', $id);
		if (!empty($where))
			$this->db->where($where);
		$result = $this->db->get('users');
		return $result->row_array();
	}
	
	//update query
	function update($where = array(), $data = array()){
		$this->db->where($where);
		$this->db->update('users', $data);
		return $this->db->affected_rows();
	}	
	
	//save data registration of a user
	function regUser($data = array())
	{
		$this->db->insert('users', $data);
		return $this->db->insert_id();
	}
	function randomPrefix($length)
	{
		$random= "";

		srand((double)microtime()*1000000);

		$data = "AbcDE123IJKLMN67QRSTUVWXYZ";
		$data .= "aBCdefghijklmn123opq45rs67tuv89wxyz";
		$data .= "0FGH45OP89";

		for($i = 0; $i < $length; $i++)
		{
			$random .= substr($data, (rand()%(strlen($data))), 1);
		}

		return $random;
	}
	
	function randomMobile_code()
	{
		$random = rand(0, 999999);
		return sprintf('%06d', $random);
	}

}