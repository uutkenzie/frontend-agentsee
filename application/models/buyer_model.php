<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Buyer_model extends CI_Model {
	
	
	function __construct() {
        parent::__construct();
    }
	
	// return all list of feature
	function getListFeature(){
		$this->db->select('id, name');
		return $this->db->get('features');
	}
	//
	function getList($perpage,$offset,$search){
		$this->db->select('suburb as value');
		$this->db->like('suburb', $search); 
		$this->db->limit($perpage,$offset);
		return $this->db->get('suburbs');
	}
	function get(){
	$this->db->select('suburb as value');
		return $this->db->get('suburbs');
	}


}