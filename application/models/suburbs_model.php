<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Suburbs_model extends CI_Model {
	
	
	function __construct() {
        parent::__construct();
    }
	
	function getList($perpage,$offset,$search){
		$this->db->select('id as suburb_id, suburb as value, state, postcode');
		$this->db->like('suburb', $search); 
		$this->db->limit($perpage,$offset);
		return $this->db->get('suburbs');
	}
	
	function getSuburb($id = 0, $where = array())
	{
		if ($id > 0)
			$this->db->where('id', $id);
		if (!empty($where))
			$this->db->where($where);
		$result = $this->db->get('suburbs');
		return $result->row_array();
	}
	
	//save data
	function add($data = array())
	{
		$this->db->insert('suburbs', $data);
		return $this->db->insert_id();
	}
}