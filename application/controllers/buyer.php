<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Buyer extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
	}
	
	// login page
	public function index()
	{
		$this->formgen->show_form($this->config->item('AddUser', 'forms'));
		$this->formgen->repopulate(array('r' => $this->input->get('r')));
		if ($this->formgen->submitted() && $this->formgen->validate())
		{
			$this->buyer_model->add();
		}
		/**paging section**/
		$totalrow = $this->buyer_model->getUserCount();
		$config['base_url'] = base_url().'buyer';
		$config['total_rows'] = $totalrow;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		$config['uri_segment'] = 4;
		$config['full_tag_open'] = '<div class="paging">';
		$config['full_tag_close'] = '</div>';
		$config['first_link'] = '&laquo;';
		$config['last_link'] = '&raquo;';
		$config['next_link'] = '&gt;';
		$config['prev_link'] = '&lt;';
		
		$this->pagination->initialize($config);
		
		$perpage = $config['per_page'];
		$offset = $this->uri->segment(2);

		$offset = ( ! is_numeric($offset) || $offset < 1) ? 0 : $offset;
		
		if(empty($offset)) {
			$offset=0;
		}
		$data['paging'] = $this->pagination->create_links();
		$data['dataList'] = $this->buyer_model->getList($perpage,$offset);
		$data['main_view'] = 'buyer/buyer';
		$this->load->view('layout', $data);
	}
	
	public function addtest()
	{		
		if ($this->formgen->submitted() && $this->formgen->validate())
		{
			$this->buyer_model->add();
		}
	}
	
	public function delete()
	{		
		$id = $this->uri->segment(3);
		$this->buyer_model->delete($id);
	}
	
	public function testupdate()
	{		
		$this->formgen->show_form($this->config->item('AddUser', 'forms'));
		$this->formgen->repopulate(array('r' => $this->input->get('r')));
		$this->load->view('buyer/update');
	}
}
