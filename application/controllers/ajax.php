<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ajax extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
	}
	public function lists(){
		echo json_encode($this->suburbs_model->getList(10,0,$_GET["term"])->result());
	}
	public function search(){
		//$this->input->post('name'); post from ajax
		//request query here
		echo 'We have 498 registered buyers with defined criteria wanting to buy in '.$this->input->post('name').'.<br> ';
	}
	public function getSuburb(){ 
		$datas = $this->suburbs_model->getSuburb(0,array('suburb' => $this->input->post('name')));
		if($datas){
			echo $datas['id'];
		}else{ echo "please select australian address";}
	}
	public function getList(){ 
		echo json_encode($this->suburbs_model->getList2(10,0,$_GET["q"])->result());
	}
}
