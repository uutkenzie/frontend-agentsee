<?php
$config['f_registration'] = array(
	'agent' => array(
		array(
			'type' => 'text',
			'label' => 'My name',
			'name' => 'firstname',
			'list_style' => 'name',
			'validation' => array('required','max_length'=>30),
			),
		array(
			'type' => 'text',
			'name' => 'lastname',
			'label' => 'lastname',
			'validation' => array('required','max_length'=>30),
			),
		array(
			'type' => 'html',
			'name' => 'clear',
			'value' => '<div class="clear"></div>',
			),
		array(
			'type' => 'text',
			'label' => 'My Agent License is',
			'name' => 'agentlicence',
			'validation' => array('trim','required'),
			),
		array(
			'type' => 'text',
			'label' => 'I work for (optional)',
			'name' => 'agencyname',
			),
		array(
			'type' => 'text',
			'label' => 'My email address is',
			'name' => 'email',
			'validation' => array('trim', 'valid_email','required'),
			),
		array(
			'type' => 'text',
			'label' => 'Confirm Email',
			'name' => 'emailc',
			'validation' => array('trim', 'valid_email', 'same_as' => 'email','required'),
			),
		array(
			'type' => 'text',
			'label' => 'My mobile number is',
			'name' => 'mobile',
			'validation' => array('trim', 'is_numeric','required', 'valid_mobile'),
			),
		array(
			'type' => 'text',
			'label' => 'Confirm mobile number',
			'name' => 'mobilec',
			'validation' => array('trim', 'is_numeric','required','same_as' => 'mobile', 'valid_mobile'),
			),
		array(
			'type' => 'password',
			'label' => 'My password will be',
			'name' => 'password',
			'validation' => array('required','trim','min_length'=>8),
			),
		array(
			'type' => 'password',
			'label' => 'Confirm password',
			'name' => 'passwordc',
			'validation' => array('required','trim','min_length'=>8,'same_as' => 'password'),
			),
		array(
			'type' => 'checkbox',
			'label' => 'I agree with the <a href="#">terms and condition</a> of Agentsee',
			'name' => 'terms',
			'list_style' => 'floatleft',
			'value' => 0,
			'preset' => 1,
			),
		array(
			'type' => 'submit',
			'name' => 'submit',
			'value' => 'Save',
			),
		),
	'buyer' => array(
		array(
			'type' => 'text',
			'label' => 'My name',
			'name' => 'firstname',
			'list_style' =>'firstname',/*Menampilkan kelas pada <li>ss</li>*/
			'validation' => array('required'),
			),
		array(
			'type' => 'text',
			'name' => 'lastname',
			'label' => '',
			'class' => 'dd',
			'list_style' =>'lastname',/*Menampilkan kelas pada <li>ss</li>*/
			'validation' => array('required'),
			),
		array(
			'type' => 'text',
			'label' => 'My email address is',
			'name' => 'email',
			'validation' => array('trim', 'valid_email','required'),
			),
		array(
			'type' => 'text',
			'label' => 'Confirm Email',
			'name' => 'emailc',
			'validation' => array('trim', 'valid_email', 'same_as' => 'email','required'),
			),
		array(
			'type' => 'text',
			'label' => 'My mobile number is',
			'name' => 'mobile',
			'validation' => array('trim', 'is_numeric','required', 'valid_mobile'),
			),
		array(
			'type' => 'text',
			'label' => 'Confirm mobile number',
			'name' => 'mobilec',
			'validation' => array('trim', 'is_numeric','required','same_as' => 'mobile', 'valid_mobile'),
			),
		array(
			'type' => 'password',
			'label' => 'My password will be',
			'name' => 'password',
			'validation' => array('required','trim','min_length'=>8),
			),
		array(
			'type' => 'password',
			'label' => 'Confirm password',
			'name' => 'passwordc',
			'validation' => array('required','trim','min_length'=>8,'same_as' => 'password'),
			),
		array(
			'type' => 'select',
			'label' => 'Primary buyer type',
			'name' => 'pbt',
			'values' => array(
				'Investor' => 'Investor',
				'First home buyer' => 'First home buyer',
				'Upgrade' => 'Upgrade',
				'Downgrader' => 'Downgrader',
				),
			'value' => 'Investor',
			'validation' => array('require'),
			),
		array(
			'type' => 'textarea',
			'label' => 'Quick description of me',
			'name' => 'description',
			'rows' => '5',
			'cols' => '50',
			'value' => '',
			),
		array(
			'type' => 'checkbox',
			'label' => 'I agree with the <a href="#">terms and condition</a> of Agentsee',
			'name' => 'terms',
			'value' => 0,
			'preset' => 1,
			),
		array(
			'type' => 'submit',
			'name' => 'submit',
			'value' => 'Save',
			),
		),
);